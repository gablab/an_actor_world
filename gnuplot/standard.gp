set xrange [-.01:1.01]
set yrange [-.01:1.01]
#unset xrange
#unset yrange

set linetype 1 pt 7
set title "Moving to form triangles"


n_blocks = `  wc -l data/energy.dat | awk '{ print $1 }' `

set terminal gif animate delay 10 size 500,500
set output "img/".filename.".gif"
#do for [i=1:n_blocks] {
do for [i=400:800] {
  pl\
  'data/'.filename.'.dat' every :::(i-2)::(i-2) ls 1 lc rgb "#A0A0A0" title "frame".(i-2),\
  'data/'.filename.'.dat' every :::(i-1)::(i-1) ls 1 lc rgb "#707070" title "frame".(i-1),\
  'data/'.filename.'.dat' every :::i::i ls 1 lc rgb "#000000" title "frame".i

  print "gnuplot working on frame ".i
}
unset output
unset terminal
