# required inputs:
# - filename
# - n_blocks

set xrange [-.01:1.01]
set yrange [-.01:1.01]
#unset xrange
#unset yrange

set linetype 1 pt 7
set title "Moving to form triangles"

set terminal gif animate delay 30 size 500,500
print filename
print "img/".filename.".gif"
set output "img/".filename.".gif"

do for [i=1:n_blocks] {
  pl\
  'data/'.filename.'.dat' every :::(i-1)::(i-1) ls 1 lc rgb "#707070" title "frame".(i-1),\
  'data/'.filename.'.dat' every :::i::i ls 1 lc 'red' title "frame".i

  print "gnuplot working on frame ".i
}
unset output
unset terminal

#'data/'.filename.'.dat' every :::(i-2)::(i-2) ls 1 lc rgb "#A0A0A0" title "frame".(i-2),\
