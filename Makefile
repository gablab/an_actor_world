# from 
# 	https://web.archive.org/web/20190403164147/https://www.gnu.org/software/make/manual/make.html:
# n.o is made automatically from n.cc, n.cpp, or n.C with a recipe of the form
# 	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c
# We encourage you to use the suffix ‘.cc’ for C++ source files instead of ‘.C’.

VPATH = src:test

CXX = c++
# CFLAGS = 
CXXFLAGS = -Isrc -std=c++11 #-Wc++11-extensions
# CPPFLAGS = 

.SUFFIXES = .o

default: World.o main.o
	$(CXX) $(CXXFLAGS) $? -o bin/an_actor_world.out


test_util: test_util.o
	$(CXX) $(CXXFLAGS) $? -o bin/test_util.out

test_geometry: test_geometry.o
	$(CXX) $(CXXFLAGS) $? -o bin/test_geometry.out

test_hello: helloworld.o 
	$(CXX) $(CXXFLAGS) $? -o bin/test_hello.out

test_actor: build_actor.o
	$(CXX) $(CXXFLAGS) $? -o bin/test_build_actor.out

test_world: World.o build_world.o 
	$(CXX) $(CXXFLAGS) $? -o bin/test_build_world.out

clean: 
	rm -f bin/*
	rm -f *.o



