# an_actor_world
[![pipeline status](https://gitlab.com/gablab/an_actor_world/badges/master/pipeline.svg)](https://gitlab.com/gablab/an_actor_world/commits/master)


This project mimics a theatre exercise intended to improve spacial awareness: more on that and on the results [here](https://gablab.gitlab.io/an_actor_world/).

## Prerequisites
C++ compiler (default is g++, see `Makefile`), gnuplot for plotting.

## Run
To execute the complete series of runs, 
```
chmod +x experiment.sh
./experiment.sh
```


