import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from math import sin

# class: Plot_multiple
#   Create a plot with multiple data entries: after initialization, call `.add_line()`. 
#   Utility to zoom: `.make_zoom()`.
#   When ready, call `.make_plot()`.
"""
# Sample use of Plot_multiple"
myplot = Plot_multiple(title="Taylor series",figure_size=(15,5))
myplot.add_line([sin(x) for x in myplot.example_x],dataname="sin(x)")
myplot.make_zoom(xrange=[0.2,0.4],yrange=[0.25,0.35])
myplot.make_plot()
"""
okabe_ito = ['#000000','#E69F00','#56B4E9','#009E73','#F0E442','#0072B2','#D55E00','#CC79A7']
c_sequential = ['#fff7fb','#ece7f2','#d0d1e6','#a6bddb','#74a9cf','#3690c0','#0570b0','#045a8d','#023858']
c_diverging =             ['#9e0142','#d53e4f','#f46d43','#fdae61','#fee08b','#ffffbf','#e6f598','#abdda4','#66c2a5','#3288bd','#5e4fa2']


class Plot_multiple:
    
    def __init__(self,title='',figure_size=(20,10),colorscheme='normal',
                 color_highlight=0,highlight_offset=0,
                 col_offset=0,path=''):
        self.fig, self.ax = plt.subplots(figsize=figure_size)
        self.i_col = 0
        self.title = title
        self.save_xdata = []
        self.save_ydata = []
        self.save_frmst = []
        self.plotpath = path
        self.col_offset = col_offset
        print("Files will be saved in "+self.plotpath)
        
        
        if colorscheme == 'normal' or colorscheme == '':
            self.colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']
        elif colorscheme == 'sequential':
            self.colors = c_sequential.copy()
        elif colorscheme == 'sequential_reverse':
            self.colors = c_sequential.copy()[::-1]
        elif colorscheme == 'colorblind':  
            self.colors = okabe_ito.copy()
        elif colorscheme == 'diverging':
            self.colors = c_diverging.copy()
        else:
            print("colorscheme not found!")
            self.colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']
            
        if col_offset > 0:    
            for i in range(col_offset):
                self.colors.pop(0)
        
        if color_highlight > 0:
            for i in range(color_highlight): # use it with sequential, to highlight the first 
                self.colors.insert(i,okabe_ito[i+highlight_offset])
                
                

                
    example_x = [i*0.001 for i in range(1,1000)]
    example_y = [(x-(x**3)/6.+(x**5)/120.) for x in example_x]
    
        
    
    def add_line(self,xdata,ydata,format_string='',dataname='series for sin(x)'):
        self.ax.plot(xdata,ydata,format_string,label=dataname,c=self.colors[self.i_col%len(self.colors)])
        self.i_col += 1
        self.save_xdata.append(xdata)
        self.save_ydata.append(ydata)
        self.save_frmst.append(format_string)
 

    def set_ranges(self,xrange=None,yrange=None):
        if xrange is not None:
            plt.xlim(xrange[0],xrange[1])
        if yrange is not None:
            plt.ylim(yrange[0],yrange[1])
    def make_plot(self,x_label='',y_label='',only_file=False):
        self.fig.legend()
        self.ax.set(xlabel=x_label,ylabel=y_label,title=self.title)
        self.ax.grid()
        self.fig.savefig(self.plotpath+self.title+".png",dpi=200)
        if(not only_file):
                plt.show()
    def make_zoom(self,coords=None,xrange=None,yrange=None):
        if coords is None:
            coords = [0.2, 0.6, .2, .2]
        a = plt.axes(coords)
        self.i_col = 0
        print(self.save_frmst)
        for (a,b,frm) in zip(self.save_xdata,self.save_ydata,self.save_frmst):
            plt.plot(a,b,frm,c=self.colors[self.i_col%len(self.colors)],label='')
            self.i_col += 1
        #plt.title('')
        self.set_ranges(xrange,yrange)
        #plt.xticks([])
        #plt.yticks([])
        
 
