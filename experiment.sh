#!/bin/bash

flag_new_analysis=0 # XXX ERASE old stats file? 1: yes; 0: no

make clean # rm bin/*; rm *.o
make # create an_actor_world.out

mkdir -p data

#--- parameters for simulation ---#
export v=0.04
export max_steps=7000
#n_range="4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 25 30 35 40 45 50 55 60 65 70 75"
#n_range="4 6 8 10 12 14 16 18 20 30 40 50 60 75"
n_range="10 "

outfile=stats/stats_$v.csv # output file of stats
#--- if new analysis, erase old file and write columns ---#
if [ $flag_new_analysis -eq 1 ]; then 
  echo "New analysis"
  echo "DELETING old stats files"
  echo "number of actors,number of steps,final energy,final average distance" > $outfile # columns
else
  echo "Continue analysis\nAppending to old files"
fi

#--- for each number of actors ---#
for n in $n_range 
do
  #--- rm data files referring to this number of actors ---#
  rm -f data/positions_$(printf %3.3d $n)*.dat
  rm -f data/energy_$(printf %3.3d $n)*.dat
  rm -f data/distance_$(printf %3.3d $n)*.dat

  #--- execute 100 simulations ---#
  echo $n "actors"
  for i in `seq 1 1000`
  do  
    output=$n","` ./run.sh $n` # catch the output of run.sh
    echo `echo $output | tr ' ' ','`  >> $outfile # make it CSV and print to stats file
  done

done

cp $outfile stats/stats_$v\_$max_steps.csv
