#!/bin/bash

N=$1
#v=0.2
#max_steps=10000

steps=`./bin/an_actor_world.out $N $max_steps $v | awk '{ print $1 }'`

#--- create a string with date+time ---#
time=`date '+%Y%m%d%H%M%S'`

#--- get energy and distance from data files ---#
# last value is retrieved (TODO make it an average)
e=`tail -1 data/energy.dat | awk '{print $2}'`
d=`tail -1 data/distance.dat | awk '{print $2}'`

#--- rename files in order to make them unique ---#
mv data/positions.dat data/positions\_$(printf %3.3d $N)\_$steps\_$v\_$time.dat
mv data/energy.dat data/energy\_$(printf %3.3d $N)\_$steps\_$v\_$time.dat
mv data/distance.dat data/distance\_$(printf %3.3d $N)_$steps\_$v\_$time.dat

#--- output ---#
echo $steps $e $d
