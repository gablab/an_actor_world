#include "util.hpp"
#include <iostream>

using std::cout;
using std::endl;

int main()
{ 
  cout << " ## random numbers ## " << endl;
  for(int i=0; i<10; ++i)
  {
    cout << rand_in_interval(0.,1.) << endl;
  }

  cout << " ## shared indices ## " << endl;
  int arr1[5] = {1,2,3,4,5};
  int arr2[4] = {8,7,1,0};
  int arr3[3] = {9,11,13};
  cout << "(T=" << true << ",F=" << false << ")\n";
  cout << "This should be true: " << arrays_share_indices(arr1,5,arr2,4) << endl;
  cout << "This should be false: " << arrays_share_indices(arr1,5,arr3,3) << endl;

  cout << " ## containing indices ## " << endl;
  cout << "This should be true: " << idx_in_array(4,arr1,5) << endl;
  cout << "This should be false: " << idx_in_array(9,arr1,5) << endl;

  return 0;
}
