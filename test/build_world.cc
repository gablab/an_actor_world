#include <iostream>
#include <fstream>
#include "World.hpp"
#include "geometry.hpp"

using namespace std;

const int N = 10;
const int steps = 10;

int main()
{
  World * w;
  ofstream ofile;

  w = new World(N);
  cout << "Creating population..." << endl;
  int w_size = w->get_size();
  cout << "Size of population: real = " << w_size << "; expected = " << N << endl;
  cout << "Printing population..." << endl;

  for(int i = 0; i < w_size; ++i)
  {
    cout << i << " ";

    cout << w->get_actor(i)->get_position();

  }


  ofile = ofstream("data/test_world_random_motion.dat");
  w->print_population(ofile);

  for(int i=0; i<steps; ++i)
  {
    w->movement(move_random<numtype>);
    w->print_population(ofile);
  }


  // now testing movement towards a point
  w = new World(N,1);
  ofile = ofstream("data/test_world_towards.dat");
  w->print_population(ofile);

  for(int i=0; i<N; ++i)
  {
    w->get_actor(i)->set_destination(0.8,0.8);
  } 

  for(int i=0; i<steps; ++i)
  {
    w->movement(move_towards<numtype>);
    w->print_population(ofile);
  }

  // now testing triangles movement 
  w = new World(4,1);
  ofile = ofstream("data/test_world_triangles.dat");
  w->print_population(ofile);


  for(int i=0; i<steps; ++i)
  {
    w->set_actors_destination((complete_equilateral<numtype>));
    w->movement(move_towards<numtype>);
    w->print_population(ofile);
    cout << "     Energy = " << w->kinetic_energy() << endl;
    cout << "     Average distance = " << w->avg_distance() << endl;
  }

  return 0;
}

