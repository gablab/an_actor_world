#include "geometry.hpp"
#include <iostream>
#include <functional>

template <class T>
void print_results_equilateral(std::vector<Position<T>*> & input, std::vector<Position<T>*> & C)
{
  using namespace std;
  cout << "A: " << *input.at(0) << "B: " << *input.at(1) << "C1: " << *C.at(0) << "C2: " << *C.at(1);
  cout << "distances: A-B = " << distance(*input.at(0),*input.at(1))
    << ", B-C1 = " << distance(*input.at(1),*C.at(0))
    << ", B-C2 = " << distance(*input.at(1),*C.at(1))
    << ", A-C1 = " << distance(*C.at(0),*input.at(0)) 
    << ", A-C2 = " << distance(*C.at(1),*input.at(0)) << endl << endl; 
}

int main()
{
  using namespace std;

  cout << " # testing the completion of an equilateral triangle # " << endl;
  vector<Position<float>*> input;
  vector<Position<float>*> C;

  create_array_of_points<float>(input,-1.,0.,1.,0.);
  complete_equilateral(input,C);
  print_results_equilateral(input,C);
  create_array_of_points<float>(input,0,-1,0,1);
  complete_equilateral(input,C);
  print_results_equilateral(input,C);
  create_array_of_points<float>(input,0.6,-14.2,2.1,3.15);
  complete_equilateral(input,C);
  print_results_equilateral(input,C);

  cout << " # testing the nearest point # " << endl;
  Position<float> A(2,2);
  create_array_of_points<float>(input,-1,-2,1.5,1.4,4,5);
  Position<float> nearest;
  nearest_point(input,A,nearest);
  cout << "nearest point (should be (1.5,1.4)) : " << nearest;




  return 0;
}

