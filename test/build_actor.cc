#include <iostream>
#include "Actor.hpp"

using namespace std;


const int n_targets = 3;



int main()
{


  Actor<float> a (0.14,0.15,0.1);
  cout << "starting position: " <<  a.get_position();
  a.move([](Position<float> r, float & k, float l){
      Position<float> p; 
      p.x = r.x + .1;
      p.y = r.y + .1;
      return p;
      });
  cout << "position increased by (.1,.1): " << a.get_position();
  a.move(move_random<float>);
  cout << "random movement: " << a.get_position();
  Position<float> destination (1.5,1.5);
  cout << "moving towards (" << destination.x << "," 
    << destination.y << "): " << endl;
  for(int i=0; i<20; ++i)
  {
    a.move(move_towards<float>,destination);
    cout << "speed is " << a.get_speed() << " ... " <<   a.get_position();
  }

  cerr << "setting index" << endl;
  a.set_idx(3);
  cerr << "choosing targets" << endl;
  a.choose_targets(n_targets,10);

  std::cerr << "targets (should not be '3'): ";
  for(int i=0; i<n_targets; ++i)
  {
    std::cerr << a.get_target(i) << " ";
  }
  std::cerr << std::endl;




  return 0;
}
