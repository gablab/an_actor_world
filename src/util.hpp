#ifndef UTIL_HPP
#define UTIL_HPP

#include <stdlib.h>
#include <time.h>
#include <random>

// TODO why 'static'? shouldn't the #ifndef/define directive be enough?

static void initialize_rand() 
{
  static int count = 0;
  if(count == 0)
  {
    count = 1;
    srand(time(NULL));
    // srand(35465768); // FOR TESTING
  }
}

// https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
  template <class T>
double rand_in_interval(T a, T b)
{
  static std::random_device rd;  //Will be used to obtain a seed for the random number engine
  static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  static std::uniform_real_distribution<> dis(a,b);
  return dis(gen);
}


  template <class T>
bool arrays_share_indices(T * arr1, int n1, T * arr2, int n2)
{
  for(int i=0; i<n1; ++i)
  {
    for(int j=0; j<n2; ++j)
    {
      if(arr1[i] == arr2[j]) return true;
    }
  }
  return false;
}

static bool idx_in_array(int x, int * arr, int n)
{
  for(int i=0; i<n; ++i)
  {
    if(x == arr[i]) return true;
  }
  return false;
}

  template <class T>
bool is_in_range(T x, T xmin, T xmax)
{
  if(x < xmin or x > xmax) return false;
  else return true;
}

  template <class T>
T bound_to_interval(T x, T xmin, T xmax)
{
  if(x>xmax) return xmax;
  else if(x<xmin) return xmin;
  else return x;
}


#endif
