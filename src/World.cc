#include "World.hpp"
#include "geometry.hpp"
#include "util.hpp"
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <functional>

World::World(int size, numtype speed)
{
  population.reserve(size);
  create_population(size,speed);
}

void World::create_population(int size, numtype speed)
{
  for(int i=0; i<size; ++i)
  { 
    population.push_back(new Actor<numtype>(
          //bound_to_interval(lx*(.5+delta*numtype(rand())/RAND_MAX),0.,lx),
          //bound_to_interval(ly*(.5+delta*numtype(rand())/RAND_MAX),0.,ly),
          //lx*numtype(rand())/RAND_MAX,ly*numtype(rand())/RAND_MAX,
          rand_in_interval(0.,lx),rand_in_interval(0.,ly),
          speed));
    population.at(i)->set_idx(i);
    population.at(i)->choose_targets(2,size);
  }
} 

void World::print_population(ofstream & file)
{
  for(auto a : population)
  {
    file << a->get_position();
  }
  file << endl;
}

void World::movement(std::function<Position<numtype>(Position<numtype>,numtype&,numtype)> f)
{
  for(auto a : population)
  {
    a->move(f);
  }
}

void World::check_bound(int step)
{
  using namespace std;
  for( auto a : population)
  {
    if(is_in_range(a->get_position().x,0.,lx) and is_in_range(a->get_position().y,0.,ly)) continue;
    else std::cerr << "out of bound (step " << step << ") : " << 
      "[ " << a->get_idx() << " ] " << a->get_position() << std::endl;
  }
}

void World::movement(std::function<Position<numtype>(Position<numtype>,Position<numtype>,numtype&,numtype)> f)
{
  for(auto a : population)
  {
    a->move(f,a->get_destination());
  }
}

void World::set_actors_destination(std::function<void(std::vector<Position<numtype>*>&,std::vector<Position<numtype>*>& )> f)
{
  for( auto a : population )
  {
    // create array of targets
    std::vector<Position<numtype>*> targets_arr;
    for(int i=0; i < a->get_n_of_targets(); ++i)
    { 
      Position<numtype> * tmp = new Position<numtype>(
          population.at(a->get_target(i))->get_position().x,
          population.at(a->get_target(i))->get_position().y);
      /* the following works only with -fpermissive!
       * targets_arr.push_back(&(population.at(a->get_target(i))->get_position()));
       */ 
      targets_arr.push_back(tmp);
    }

    // find the destination points with the input function
    std::vector<Position<numtype>*> output_points;
    f(targets_arr,output_points);

    // find the nearest destination to reach
    Position<numtype> nearest;
    nearest_point(output_points,a->get_position(),nearest);

    // rectangle boundaries
    ///*
    numtype & xp = nearest.x;
    numtype & yp = nearest.y;
    if(yp>ly) yp = ly;
    else if(yp<0.) yp = 0.;
    if(xp>lx) xp = lx;
    else if(xp<0.) xp = 0.;
    if(is_in_range(xp,0.,lx) and is_in_range(yp,0.,ly)) {} else 
      // ==== if(is_in_range(nearest.x,0.,lx) and is_in_range(nearest.y,0.,ly)) {} else 
      std::cerr << "destination out of range: " << xp << " " << yp << endl;
    //*/

    a->set_destination(nearest.x,nearest.y);
  }
}

#include <iostream>
numtype World::kinetic_energy()
{
  using namespace std;
  numtype E = 0;
  for( auto a : population)
  {
    //cerr << a->get_speed() << endl;
    E += std::pow(a->get_speed(),2);
  }
  E /= 2.;
  return E;
}

numtype World::avg_distance()
{
  numtype avg_distance = 0;
  int count = 0;
  for( auto a : population)
  {
    for( auto b : population)
    {
      if(a->get_idx() == b->get_idx()) continue;
      Position<numtype> * tmp_a = new Position<numtype>(
          a->get_position().x, a->get_position().y);
      Position<numtype> * tmp_b = new Position<numtype>(
          b->get_position().x, b->get_position().y);
      avg_distance += distance(tmp_a, tmp_b);
      ++count;
    }
  }
  return (avg_distance / count);
}



