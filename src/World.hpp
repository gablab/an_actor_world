#ifndef WORLD_HPP
#define WORLD_HPP

#include <vector>
#include "Actor.hpp"
#include <fstream>



using namespace std;

typedef double numtype;
//typedef float actortype;

class World {

  private:
    vector<Actor<numtype>*> population;
    numtype lx = 1.;
    numtype ly = 1.;

  public:
    World(int, numtype speed=0.01);//,numtype,numtype);

    void set_rectangle(numtype x, numtype y) { lx = x; ly = y; } 

    void create_population(int size, numtype speed=0.01);
    void print_population(ofstream &);
    int get_size() { return population.size(); }
    Actor<numtype>* get_actor(int i) { return population.at(i); }

    void movement(std::function<Position<numtype>(Position<numtype>,numtype&,numtype)>);
    void movement(std::function<Position<numtype>(Position<numtype>,Position<numtype>,numtype&,numtype)>);

    // thanks to Enrico Lusiani, Francesco Forcher for the help
    void set_actors_destination(std::function<void(std::vector<Position<numtype>*>&,
        std::vector<Position<numtype>*>&
        )>);

    numtype kinetic_energy();
    numtype avg_distance();
    void check_bound(int);

};

#endif
