#include <iostream>
#include <fstream>
#include <string>
#include "World.hpp"
#include "geometry.hpp"

using namespace std;

/*const int N = 4;
  const int steps = 30;
  const numtype delta = 0.5;
  const numtype speed = 0.01;*/

int main(int argc, char** argv)
{
  const int N = stoi(argv[1]);
  const int steps = stoi(argv[2]);
  const numtype speed = stof(argv[3]);

  World * w = new World(N,speed);

  ofstream ofile = ofstream("data/positions.dat");
  w->print_population(ofile);
  ofstream efile = ofstream("data/energy.dat");
  ofstream dfile = ofstream("data/distance.dat");


  int count = 0;
  int last_step;
  for(int i=0; i<steps; ++i)
  {
    w->set_actors_destination((complete_equilateral<numtype>));
    w->movement(move_towards<numtype>);
    w->check_bound(i);
    //w->movement(move_random<numtype>);
    w->print_population(ofile);
    numtype D = w->avg_distance();
    dfile << i << " " << D << endl;
    numtype E = w->kinetic_energy();
    efile << i << " " << E << endl;
    if(E<1.e-5*speed*speed){
      ++count;
    }
    else count = 0;
    last_step = i;
    if(count > 10) break;    
  }

  cout << last_step << " steps" << endl;

  return 0;
}

