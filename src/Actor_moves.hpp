#ifndef ACTOR_MOVES_HPP
#define ACTOR_MOVES_HPP

#include "Position.hpp"
#include "util.hpp"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h>
#include <iostream>



  template <class T>
Position<T> move_random(Position<T> r, T & v, T v_max)
{
  Position<T> p;
  //initialize_rand();
  p.x = r.x + rand_in_interval(0.,v/10.);
  p.y = r.y + rand_in_interval(0.,v/10.);
  return p;
}

  template <class T>
Position<T> move_towards(Position<T> r, Position<T> q, T & v, T v_max)
{
  Position<T> p;
  T vx, vy;

  // don't move if you are too near; once in three times "take a moment to think"
  T distance = std::sqrt(std::pow(q.y-r.y,2)+std::pow(q.x-r.x,2));
  if(distance < 1.e-06 or rand_in_interval(0.,1.) < 1./3) 
  {
    v = 0;
    p.x = r.x;
    p.y = r.y;
    return p; 
  }

  // don't go beyond the destination
  if(v_max>distance)
  {
    v = distance;
    p.x = q.x;
    p.y = q.y;
    return p;
  }

  v = v_max;

  // special cases
  if(std::abs(r.x - q.x) < 1.e-12) 
  {
    p.x = r.x;
    p.y = r.y + v*(q.y>r.y?1.:-1.);
  } 
  else if(std::abs(r.y - q.y) < 1.e-12)
  {
    p.x = r.x + v*(q.x>r.x?1.:-1.);
    p.y = r.y;
  } 
  else
  {
    T tan_theta = (q.y-r.y)/(q.x-r.x);
    T tmp = v/std::sqrt(1+std::pow(tan_theta,2));
    p.x = r.x + tmp*(q.x>r.x?1.:-1.);
    p.y = r.y + tmp*std::abs(tan_theta)*(q.y>r.y?1.:-1.);

    if(std::abs(tan_theta) < 1.e-12 or std::abs(tan_theta) > 1.e12) 
      std::cerr << "move_towards: singular tangent, " << tan_theta
        << "( from " << r.x << " " << r.y  
        << " , destination: " << q.x << " " << q.y << " , distance " << distance << ") \n";
  }


  if(std::abs( ((p.x-r.x)*(p.x-r.x) + (p.y-r.y)*(p.y-r.y)) / (v*v) - 1.)> 1.e-6) 
    std::cerr << "move_towards: actual distance travelled " 
      << ((p.x-r.x)*(p.x-r.x) + (p.y-r.y)*(p.y-r.y))
      << " != " << v*v << std::endl;

  // is this right? No: the point can be projected on the boundary if outside.
  /*if(std::atan((p.y-r.y)/(p.x-r.x))/((q.y-r.y)/(q.x-r.x)) - 1. > 1.e-6)
    std::cerr << "move_towards: error in angle of movement" << std::endl;*/

  // check rectangular boundaries
  /*
   * if(is_in_range(p.x,(T)0.,(T)1.) and is_in_range(p.y,(T)0.,(T)1.)) {} else {
    std::cerr << "move_towards: move out of range: " << p.x << " " << p.y << std::endl;
    std::cerr << "( from " << r.x << " " << r.y 
      << " , destination: " << q.x << " " << q.y << " , distance " << distance << ") \n";

  }
  */

  return p;
}

#endif
