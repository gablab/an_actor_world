#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include "Position.hpp"
#include <vector>
#include <math.h>

/* find the two 'C' points which complete an equilateral triangle given A and B
*/
  template <class T>
void complete_equilateral(std::vector<Position<T>*> & input_points, std::vector<Position<T>*> & arr)
{
  Position<T> A;
  A.x = input_points.at(0)->x;
  A.y = input_points.at(0)->y;
  Position<T> B;
  B.x = input_points.at(1)->x;
  B.y = input_points.at(1)->y;
  T delta_x = B.x - A.x;
  T delta_y = B.y - A.y;
  T x_M = (A.x + B.x)/2.;
  T y_M = (A.y + B.y)/2.;
  T d = std::sqrt( (std::pow(delta_x,2) + std::pow(delta_y,2)) * 3./4. );
  T dx;
  T dy;
  if(delta_x == .0)
  {
    dx = d;
    dy = 0.;
  } else if(delta_y == .0)
  { 
    dx = 0.;
    dy = d;
  } else
  { 
    T a = - delta_x/delta_y;
    dx = d/std::sqrt(1+std::pow(a,2));
    dy = dx*a;
  }

  arr.resize(0);
  Position<T> * C1 = new Position<T>(x_M+dx,y_M+dy);
  Position<T> * C2 = new Position<T>(x_M-dx,y_M-dy);
  arr.push_back(C1); 
  arr.push_back(C2); 
}

  template <class T>
T distance(Position<T> A, Position<T> B)
{
  return std::sqrt( std::pow(A.x-B.x,2) + std::pow(A.y-B.y,2) );
}

  template <class T>
T distance(Position<T> * A, Position<T> * B)
{
  return std::sqrt( std::pow(A->x - B->x,2) + std::pow(A->y - B->y,2) );
}
 
  template <class T>
void nearest_point(
    std::vector<Position<T>*> input_points, Position<T> actual, Position<T> & nearest)
{
  using namespace std;

  nearest = *input_points.at(0);
  T dist = distance(*input_points.at(0),actual);
  for( auto a : input_points)
  {
    T tmp = distance(*a,actual);
    if(tmp < dist) {
      dist = tmp;
      nearest.x = a->x;
      nearest.y = a->y;
    }
  }
}

#endif
