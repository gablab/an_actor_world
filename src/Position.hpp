#ifndef POSITION_HPP
#define POSITION_HPP

#include <iostream>
#include <vector>


template <class T>
class Position
{
  public:
    Position(T x_in, T y_in): x(x_in), y(y_in) {}
    Position() {};
    T x,y;
    void operator =(const Position<T> p) {
      x = p.x;
      y = p.y;
    }
};

  template <class T>
std::ostream& operator<<(std::ostream& os, const Position<T> & p)
{
  os << p.x << " " << p.y << std::endl;
  return os;
}

// TODO : can those be generalized?
/*
  template <class T>
void create_array_of_points(std::vector<Position<T>*> & arr, T Ax, T Ay)
{
  arr.resize(0);
  Position<T> * A = new Position<T>(Ax,Ay);
  arr.push_back(A);
}*/

  template <class T>
void create_array_of_points(std::vector<Position<T>*> & arr, T Ax, T Ay, T Bx, T By)
{
  arr.resize(0);
  Position<T> * A = new Position<T>(Ax,Ay);
  arr.push_back(A);
  Position<T> * B = new Position<T>(Bx,By);
  arr.push_back(B);
}

  template <class T>
void create_array_of_points(std::vector<Position<T>*> & arr, T Ax, T Ay, T Bx, T By, T Cx, T Cy)
{
  arr.resize(0);
  create_array_of_points(arr,Ax,Ay,Bx,By);
  Position<T> * C = new Position<T>(Cx,Cy);
  arr.push_back(C);
}
 /* 
  template <class T>
void add_point_to_array(std::vector<Position<T>*> & arr, T Cx, T Cy)
{
  Position<T> * C = new Position<T>(Cx,Cy);
  arr.push_back(C);
}*/


#endif
