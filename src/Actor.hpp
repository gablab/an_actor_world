/* It seems you cannot split the definition from the declaration; see
 * https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
 * https://stackoverflow.com/questions/36039/templates-spread-across-multiple-files
 */

#ifndef ACTOR_HPP
#define ACTOR_HPP

#include "Position.hpp"
#include "Actor_moves.hpp"
#include <functional>
#include <iostream>

template <class T>
class Actor 
{
  public: 
    Actor(T,T);
    Actor(T,T,T);
    //~Actor();
    Position<T> get_position() {return r;}
    //T get_position_x();
    //T get_position_y();
    void set_destination(T x, T y) {destination.x = x; destination.y = y;}
    void move(std::function<Position<T>(Position<T>,T&,T)>);
    void move(std::function<Position<T>(Position<T>,Position<T>,T&,T)>, Position<T>);
    void choose_targets(int,int);
    int get_n_of_targets() {return number_of_targets;}
    int get_target(int i) {return target[i];}
    Position<T> get_destination() {return destination;}
    void set_idx(int i) {idx = i;}
    int get_idx() {return idx;}
    T get_max_speed() {return max_speed;}
    T get_speed() {return speed;}

  private: 
    T speed;
    T max_speed = 1.;
    Position<T> r;
    Position<T> destination;
    int idx;
    int number_of_targets;
    int * target;
};

  template <class T>
Actor<T>::Actor(T x_in, T y_in)
{
  r.x = x_in;
  r.y = y_in;
}
  template <class T>
Actor<T>::Actor(T x_in, T y_in, T v)
{
  r.x = x_in;
  r.y = y_in;
  max_speed = v;
}

  template <class T>
void Actor<T>::move(std::function<Position<T>(Position<T>,T&,T)> func)
{
  r = func(r,speed,max_speed);
}

  template <class T>
void Actor<T>::move(std::function<Position<T>(Position<T>,Position<T>,T&,T)> func, 
    Position<T> destination)
{
  r = func(r,destination,speed,max_speed);
}

  template <class T>
void Actor<T>::choose_targets(int n_targets, int n_actors)
{
  number_of_targets = n_targets;
  int * prohibited_idx = new int[n_targets];
  prohibited_idx[0] = idx;
  for(int i=1; i<n_targets; ++i) prohibited_idx[i] = -1;
  target = new int[n_targets];
  int tmp;
  for(int i=0; i<n_targets; ++i)
  {
    do
    {
      tmp = T(n_actors)*rand_in_interval(float(0.),float(1.));//float(rand())/RAND_MAX;
    } while(idx_in_array(tmp,prohibited_idx,n_targets));
    target[i] = tmp;
    prohibited_idx[i+1] = tmp;
  }
}





#endif
